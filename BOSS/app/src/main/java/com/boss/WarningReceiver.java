package com.boss;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;

public class WarningReceiver extends BroadcastReceiver{
    private static final String CHANNEL_ID = "rangerWarning";

    @Override
    public void onReceive(Context context, Intent intent) {
        final DatabaseAccess db = DatabaseAccess.getInstance(context.getApplicationContext());
        String userEmail = intent.getStringExtra("EMAIL");
        db.open();
        // check current walk status for user email
        boolean checkActiveRanger = db.checkIsRangerActive();
        boolean checkWalkStatus = db.checkWalkStatus(userEmail);
        // check if the user_type ranger is active

        if (checkWalkStatus && checkActiveRanger) {
            Intent notificationIntent = new Intent(context, RangerAnalyticsActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(NotificationActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.Builder builder = new Notification.Builder(context);

            Notification notification = builder.setContentTitle("Ranger Alert!")
                    .setContentText(userEmail+" has not checked in")
                    .setSmallIcon(R.drawable.ic_warning)
                    .setContentIntent(pendingIntent).build();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(CHANNEL_ID);
            }

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(
                        CHANNEL_ID,
                        "NotificationDemo",
                        IMPORTANCE_DEFAULT
                );
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(0, notification);
        }
    }
}