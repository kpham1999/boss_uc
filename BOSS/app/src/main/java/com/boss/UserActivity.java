package com.boss;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class UserActivity extends AppCompatActivity {
    Cursor cursor;

    String[] trailName;
    String[] trailStartTime;
    String[] trailDuration;
    String[] walkStatus;
    String[] trailEndTime;

    EditText editTextPhone,editTextEmergency,editTextMedical;
    TextView textViewUsername,textViewEmail;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Intent recieveIntent = getIntent();
        final String userEmail = recieveIntent.getStringExtra("EMAIL");
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        // get walkedID
        final int walkerID = db.getUserID(userEmail);
        // Get UserActivity Trail using walkerID
        cursor = db.getUserActivityTrail(walkerID);

        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextEmergency = (EditText) findViewById(R.id.editTextEmergency);
        editTextMedical = (EditText) findViewById(R.id.editTextMedical);
        textViewUsername = (TextView) findViewById(R.id.textViewUsername);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        cursor = db.setUserActData(walkerID);
        if (cursor.getCount() == 0) {
            Toast.makeText(UserActivity.this, "No info!", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                editTextPhone.setText(cursor.getString(0));
                editTextEmergency.setText(cursor.getString(1));
                editTextMedical.setText(cursor.getString(2));
                textViewUsername.setText(cursor.getString(3));
                textViewEmail.setText(cursor.getString(4));
            }
        }


        CardView cardViewSave = findViewById(R.id.cardViewSave);
        cardViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get current values in edittext
                String phone = editTextPhone.getText().toString();
                String emergency = editTextEmergency.getText().toString();
                String medical = editTextMedical.getText().toString();
                // If any field is empty
                if(phone.isEmpty() || emergency.isEmpty()||medical.isEmpty()) {
                    Toast.makeText(UserActivity.this, "Please enter all the fields!",
                            Toast.LENGTH_SHORT).show();
                } else {
                    // Insert updated data into db
                    Boolean updatInfo = db.updateInformation(phone,emergency,medical,walkerID);
                    if(updatInfo) {
                        Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
                        intent.putExtra("EMAIL", userEmail);
                        startActivity(intent);
                    }
                }
            }
        });

        CardView cardViewCancel = findViewById(R.id.cardViewCancel);
        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
                intent.putExtra("EMAIL", userEmail);
                startActivity(intent);
            }
        });


        LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout registeredWalks = (LinearLayout) findViewById(R.id.linearLayoutRegisteredWalks);
        cursor = db.getUserActivityTrail(walkerID);
        // Init array
        trailName = new String[cursor.getCount()];
        trailStartTime = new String[cursor.getCount()];
        trailEndTime = new String[cursor.getCount()];
        trailDuration = new String[cursor.getCount()];
        walkStatus = new String[cursor.getCount()];
        // Move cursor to first row
        cursor.moveToFirst();
        for (int i=0; i<trailName.length; i++) {
            final View trail = layoutInflater.inflate(R.layout.user_registered_walk_item, null);
            // Components to have values set
            TextView name = trail.findViewById(R.id.name);
            TextView startTime = trail.findViewById(R.id.startTime);
            TextView duration = trail.findViewById(R.id.duration);
            TextView actionButtonText = trail.findViewById(R.id.actionButtonText);

            // Components to be interacted with
            CardView actionButton = trail.findViewById(R.id.actionButton);

            // Setting components values
            trailName[i] = cursor.getString(0);
            name.setText(trailName[i]);
            trailStartTime[i] = "Registered time: " + cursor.getString(1)
                    + " - " + cursor.getString(3);
            startTime.setText(trailStartTime[i]);
            trailDuration[i] = cursor.getString(2);
            duration.setText(trailDuration[i] + " mins");
            walkStatus[i] = cursor.getString(4);
            final String walkID = cursor.getString(5);

            cursor.moveToNext();

            if (walkStatus[i].equals("completed")) {
                trail.findViewById(R.id.registeredWalkItem).setBackgroundColor(Color.parseColor("#80999999"));
                actionButton.setCardBackgroundColor(Color.parseColor("#0FDA5B"));
                actionButtonText.setText("Feedback");
                actionButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // Once clicked move user to the active trail activity
                        Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                        intent.putExtra("WALK_ID", walkID);
                        intent.putExtra("EMAIL", userEmail);
                        startActivity(intent);
                    }
                });

            } else {
                actionButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // Once clicked move user to trail details activity
                        Intent intent = new Intent(getApplicationContext(), ActiveWalk.class);
                        intent.putExtra("WALK_ID", walkID);
                        intent.putExtra("EMAIL", userEmail);
                        startActivity(intent);
                    }
                });
            }

            registeredWalks.addView(trail);
        }
    }
}