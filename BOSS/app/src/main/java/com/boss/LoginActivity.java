package com.boss;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class LoginActivity extends AppCompatActivity {
    EditText editTextEmail, editTextPassword;
    CardView cardViewLogin;
    TextView textViewRegister;

    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Declare database
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        // Reset active status of users to inactive
        Boolean resetActive = db.resetActive();
        if(resetActive)  {
        }
        setContentView(R.layout.activity_login);
        // Edit Text
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        // Button
        cardViewLogin = (CardView) findViewById(R.id.cardViewLogin);
        textViewRegister = (TextView) findViewById(R.id.textViewRegister);

        // Log in button onlick
        cardViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open database
                db.open();
                String email = editTextEmail.getText().toString().toLowerCase();
                String password = editTextPassword.getText().toString();
                String hashedPassword = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
                // If fields are empty
                if(email.isEmpty()|| password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Please enter all the fields",
                            Toast.LENGTH_SHORT).show();
                } else {
                    // Check credentials with email and password in the database
                    Boolean checkEmail = db.checkCreds(email,hashedPassword);
                    Boolean checkRanger = db.checkRanger(email);
                    // If email is valid
                    if(checkEmail) {
                        // if ranger is valid
                        if(checkRanger) {
                            String rangerName = db.getRangerName(email);
                            boolean isActive = db.updateActive(email);
                            if(isActive) {
                                // Display welcome message to ranger with their name
                                Toast.makeText(LoginActivity.this, "Welcome " + rangerName,
                                        Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),
                                        RangerAnalyticsActivity.class);
                                intent.putExtra("EMAIL", email);
                                startActivity(intent);
                                db.close();
                            }
                        }
                        // if user is valid
                        else {
                            boolean isActive = db.updateActive(email);
                            if(isActive) {
                                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                intent.putExtra("EMAIL",email);
                                startActivity(intent);
                                db.close();
                            }
                        }
                    } else {
                        // else credentials are valid
                        Toast.makeText(LoginActivity.this, "Invalid credentials",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        // Register Button
        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editTextEmail.getText().toString();
                // Once clicked move user to register
                Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
                intent.putExtra("EMAIL", email);
                startActivity(intent);
            }
        });
    }
    public void openMapMenu(View view) {

    }
}