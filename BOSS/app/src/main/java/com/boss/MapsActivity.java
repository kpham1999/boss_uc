package com.boss;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentUserLocationMarker;
    private static final int Request_User_Location_Code = 99;

    // Menu Park Data
    final String pNames[] = {"Stromlo Park", "University of Canberra", "Arboretum"};
    final String pDescriptions[] = {"Sporting venue with a cycling circuit & grass running track, plus equestrian & mountain bike trails", "One of Australia's Leading Universities", "A mosaic of living forests and gardens"};
    final int pImages[] = {R.drawable.parks_stromlo, R.drawable.parks_uc, R.drawable.parks_arboretum};
    final LatLng pPosition[] = {new LatLng(-35.320068, 149.025592), new LatLng(-35.2384551, 149.0844455), new LatLng(-35.287934, 149.069272)};
    PolylineOptions pTrailOptions[][] = {
            {
                    // Mountain Trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.320067, 149.027006))
                            .add(new LatLng(-35.319157, 149.026901))
                            .add(new LatLng(-35.319008, 149.026971))
                            .add(new LatLng(-35.318627, 149.027963))
                            .add(new LatLng(-35.318474, 149.029106))
                            .add(new LatLng(-35.318540, 149.029503))
                            .add(new LatLng(-35.318641, 149.029621))
                            .add(new LatLng(-35.318799, 149.029707))
                            .add(new LatLng(-35.319018, 149.029723))
                            .add(new LatLng(-35.319197, 149.029723))
                            .add(new LatLng(-35.319455, 149.029594))
                            .add(new LatLng(-35.319709, 149.029358))
                            .add(new LatLng(-35.320077, 149.028387))
                            .add(new LatLng(-35.319836, 149.028264))
                            .add(new LatLng(-35.319748, 149.028200))
                            .add(new LatLng(-35.319761, 149.028093))
                            .add(new LatLng(-35.319857, 149.028023))
                            .add(new LatLng(-35.319975, 149.028034))
                            .add(new LatLng(-35.320408, 149.028168))
                            .add(new LatLng(-35.320636, 149.028120))
                            .add(new LatLng(-35.321034, 149.027833))
                            .add(new LatLng(-35.321235, 149.027567))
                            .add(new LatLng(-35.321248, 149.027326))
                            .add(new LatLng(-35.321160, 149.027079))
                            .add(new LatLng(-35.321022, 149.026918))
                            .add(new LatLng(-35.320067, 149.027006))

                    ,
                    // Dirt Trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.320837, 149.024906))
                            .add(new LatLng(-35.320644, 149.022353))
                            .add(new LatLng(-35.320701, 149.021871))
                            .add(new LatLng(-35.320627, 149.021420))
                            .add(new LatLng(-35.320334, 149.020985))
                            .add(new LatLng(-35.318777, 149.019848))
                            .add(new LatLng(-35.317219, 149.019194))
                            .add(new LatLng(-35.316846, 149.019128))
                            .add(new LatLng(-35.315934, 149.020126))
                            .add(new LatLng(-35.316266, 149.020977))
                            .add(new LatLng(-35.316304, 149.022591))
                            .add(new LatLng(-35.316176, 149.023781))
                            .add(new LatLng(-35.316229, 149.024553))
                            .add(new LatLng(-35.316824, 149.025304))
                            .add(new LatLng(-35.320334, 149.020985))
            },
            {
                    // Refectory trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.238467, 149.084275))
                            .add(new LatLng(-35.236984, 149.084243))
                            .add(new LatLng(-35.236937, 149.084299))
                            .add(new LatLng(-35.236781, 149.084300))
                            .add(new LatLng(-35.236746, 149.084382))
                            .add(new LatLng(-35.236537, 149.084489)),
                    // Bus stop to library trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.242317, 149.086260))
                            .add(new LatLng(-35.242321, 149.085423))
                            .add(new LatLng(-35.241509, 149.085379))
                            .add(new LatLng(-35.241507, 149.085245))
                            .add(new LatLng(-35.241351, 149.085245))
                            .add(new LatLng(-35.241296, 149.085173))
                            .add(new LatLng(-35.241268, 149.084240))
                            .add(new LatLng(-35.240760, 149.084261))
                            .add(new LatLng(-35.240561, 149.084280))
                            .add(new LatLng(-35.240171, 149.084247))
                            .add(new LatLng(-35.239724, 149.084263))
                            .add(new LatLng(-35.238563, 149.084187))
                            .add(new LatLng(-35.238521, 149.083986))
                            .add(new LatLng(-35.238260, 149.083715))
                            .add(new LatLng(-35.238085, 149.083691))
            },
            {
                    // Gravel Trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.291870, 149.064191))
                            .add(new LatLng(-35.291920, 149.064188))
                            .add(new LatLng(-35.291954, 149.064179))
                            .add(new LatLng(-35.291990, 149.064172))
                            .add(new LatLng(-35.292034, 149.064167))
                            .add(new LatLng(-35.292073, 149.064180))
                            .add(new LatLng(-35.292117, 149.064221))
                            .add(new LatLng(-35.292143, 149.064276))
                            .add(new LatLng(-35.292166, 149.064357))
                            .add(new LatLng(-35.292179, 149.064507))
                            .add(new LatLng(-35.292182, 149.064558))
                            .add(new LatLng(-35.292068, 149.064563))
                            .add(new LatLng(-35.292182, 149.064558))
                            .add(new LatLng(-35.292226, 149.064604))
                            .add(new LatLng(-35.292283, 149.064628))
                            .add(new LatLng(-35.292379, 149.064634))
                    ,

                    // Grass Trail
                    new PolylineOptions().color(Color.rgb(187, 187, 187))
                            .add(new LatLng(-35.286953, 149.068193))
                            .add(new LatLng(-35.286912, 149.068570))
                            .add(new LatLng(-35.286988, 149.068744))
                            .add(new LatLng(-35.286880, 149.068921))
                            .add(new LatLng(-35.287107, 149.069086))
                            .add(new LatLng(-35.286826, 149.069139))
                            .add(new LatLng(-35.287084, 149.069279))
                            .add(new LatLng(-35.286799, 149.069363))
                            .add(new LatLng(-35.287076, 149.069478))
                            .add(new LatLng(-35.286826, 149.069588))
                            .add(new LatLng(-35.287083, 149.069653))
                            .add(new LatLng(-35.286864, 149.069788))
                            .add(new LatLng(-35.287090, 149.069825))
                            .add(new LatLng(-35.286917, 149.069959))
                            .add(new LatLng(-35.287121, 149.069999))
                            .add(new LatLng(-35.287168, 149.069936))
                            .add(new LatLng(-35.287475, 149.069960))
                            .add(new LatLng(-35.287736, 149.070075))
                            .add(new LatLng(-35.287880, 149.070142))
                            .add(new LatLng(-35.288138, 149.070155))
                            .add(new LatLng(-35.288296, 149.070254))
                            .add(new LatLng(-35.288296, 149.070254))
                            .add(new LatLng(-35.288909, 149.069753))
                            .add(new LatLng(-35.289102, 149.069732))
                            .add(new LatLng(-35.289426, 149.070161))
                            .add(new LatLng(-35.289772, 149.070362))
                            .add(new LatLng(-35.290111, 149.070239))
                            .add(new LatLng(-35.290330, 149.069872))
                            .add(new LatLng(-35.290912, 149.068526))
                            .add(new LatLng(-35.291098, 149.068335))
                            .add(new LatLng(-35.288473, 149.067955))



            }
    };
    Polyline[][] pTrails = new Polyline[pTrailOptions.length][];

    // Menu Trail Data
    final String tNames[][] = {{"Mountain Trail", "Dirt Trail"}, {"Refectory Trail", "Bus To Library Trail"}, {"Gravel Trail", "Grass Trail"}};
    final int tRatings[][] = {{4, 3}, {2, 4}, {4, 3}};
    final String tDifficulties[][] = {{"Hard", "Easy"}, {"Medium", "Easy"}, {"Hard", "Easy"}};
    final String tDistances[][] = {{"50km", "30km"}, {"43km", "20km"}, {"50km", "30km"}};
    final String tDurations[][] = {{"120m", "60m"}, {"110m", "10m"}, {"120m", "50m"}};


    ListView pListView;

    // Menu Variables
    final LatLng defaultPosition = new LatLng(-35.3059519, 149.1225202);
    Polyline activeTrail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent recieveIntent = getIntent();
                String email = recieveIntent.getStringExtra("EMAIL");

                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra("EMAIL", email);
                startActivity(intent);
            }
        });

        // Call check location permission function
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkUserLoationPermission();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //Define listview and its adapter
        pListView = (ListView) findViewById(R.id.listViewMenu);
        ParksAdapter adapter = new ParksAdapter(this, pNames);
        pListView.setAdapter(adapter);
        // On click for list item to change map position
        pListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CameraPosition cameraPosition = CameraPosition.builder()
                        .target(pPosition[position])
                        .zoom(15)
                        .bearing(0)
                        .build();

                // Animate the change in camera view over 2 seconds
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                        2000, null);

                Toast.makeText(MapsActivity.this, pNames[position], Toast.LENGTH_SHORT).show();
            }

        });

    }

    class ParksAdapter extends ArrayAdapter<String> {
        Context context;

        ParksAdapter(Context c, String name[]) {
            super(c, R.layout.menu_item, R.id.name, name);
            this.context = c;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
            final int pPosition = position;

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View menu_item = layoutInflater.inflate(R.layout.menu_item, parent, false);

            // Components to have values set
            ImageView images = menu_item.findViewById(R.id.imageView);
            TextView myName = menu_item.findViewById(R.id.name);
            TextView mySubtitle = menu_item.findViewById(R.id.subtitle);

            // Components to be interacted with
            final Button arrowButton = menu_item.findViewById(R.id.arrowBtn);
            final ConstraintLayout expandableView = menu_item.findViewById(R.id.expandableView);
            final CardView cardView = menu_item.findViewById(R.id.cardView);
            final ImageView parkImage = menu_item.findViewById(R.id.imageView);

            // Setting components values
            images.setImageResource(pImages[pPosition]);
            myName.setText(pNames[pPosition]);
            mySubtitle.setText(pDescriptions[pPosition]);

            // On click for expander
            arrowButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (expandableView.getVisibility() == View.GONE) {
                        TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                        expandableView.setVisibility(View.VISIBLE);
                        arrowButton.setBackgroundResource(R.drawable.ic_arrow_up);
                    } else {
                        TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                        expandableView.setVisibility(View.GONE);
                        arrowButton.setBackgroundResource(R.drawable.ic_arrow_down);
                    }
                }
            });

            int size = tNames[pPosition].length;

            final LinearLayout trails = (LinearLayout) menu_item.findViewById(R.id.trailsLinearLayout);
            for (int i = 0; i < size; i++) {
                final View trail = layoutInflater.inflate(R.layout.menu_trail, null);
                final int tPosition = i;

                // Components to have values set
                TextView name = trail.findViewById(R.id.name);
                RatingBar rating = trail.findViewById(R.id.rating);
                TextView difficulty = trail.findViewById(R.id.difficulty);
                TextView distance = trail.findViewById(R.id.distance);
                TextView duration = trail.findViewById(R.id.duration);


                // Components to be interacted with
                CardView detailsButton = trail.findViewById(R.id.detailsButton);
                RelativeLayout trailItem = trail.findViewById(R.id.trailItem);

                // Setting components values
                name.setText(tNames[pPosition][tPosition]);
                rating.setRating(tRatings[pPosition][tPosition]);
                difficulty.setText(tDifficulties[pPosition][tPosition]);
                distance.setText(tDistances[pPosition][tPosition]);
                duration.setText(tDurations[pPosition][tPosition]);

                trailItem.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // Setting menu highlighted item
                        for (int i = 0; i < pNames.length - 1; i++) {
                            View parkView = pListView.getChildAt(i);
                            ViewGroup parkTrails = parkView.findViewById(R.id.trailsLinearLayout);

                            for (int index = 0; index < ((ViewGroup) parkTrails).getChildCount(); index++) {
                                View nextChild = ((ViewGroup) parkTrails).getChildAt(index);
                                nextChild.findViewById(R.id.trailItem).setBackgroundColor(getResources().getColor(R.color.gradientPrimary));
                            }
                        }

                        trail.findViewById(R.id.trailItem).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

                        // Set active trail colours
                        if (activeTrail != null) {
                            activeTrail.setColor(Color.rgb(187, 187, 187));
                        }
                        activeTrail = pTrails[pPosition][tPosition];
                        pTrails[pPosition][tPosition].setColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                });

                detailsButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        int idCounter = 1;
                        outerloop:
                        for (int i = 0; i < pNames.length; i++) {
                            for (int j = 0; j < tNames[i].length; j++) {
                                if ((i == pPosition) && (j == tPosition)) {
                                    break outerloop;
                                } else {
                                    idCounter++;
                                }
                            }
                        }

                        Intent recieveIntent = getIntent();
                        String email = recieveIntent.getStringExtra("EMAIL");

                        // Once clicked move user to trail details activity
                        Intent intent = new Intent(getApplicationContext(), TrailDetailsActivity.class);
                        intent.putExtra("ID", idCounter);
                        intent.putExtra("EMAIL", email);
                        startActivity(intent);
                    }
                });

                trails.addView(trail);
            }
            return menu_item;
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(mMap.MAP_TYPE_SATELLITE);
//        mMap.setMapType(mMap.MAP_TYPE_TERRAIN);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();

            mMap.setMyLocationEnabled(true);
        }

        // Setting default map position
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultPosition, 10));

        // Getting the largest amount of trails a park has
        int biggest = 0;
        for (int i = 0; i < pTrailOptions.length; i++) {
            if (pTrailOptions[i].length > biggest) {
                biggest = pTrailOptions[i].length;
            }
        }

        // Defining trails
        pTrails = new Polyline[pTrailOptions.length][biggest];

        // Adding trails to map
        for (int j = 0; j < pTrailOptions.length; j++) {
            for (int k = 0; k < pTrailOptions[j].length; k++) {
                pTrails[j][k] = this.mMap.addPolyline(pTrailOptions[j][k]);
            }
        }

        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
        rlp.setMargins(0, 630, 180, 0);
    }

    public boolean checkUserLoationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Request_User_Location_Code:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (googleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;

        //Add marker to user's location
//        if (currentUserLocationMarker != null)
//        {
//            currentUserLocationMarker.remove();
//        }
//
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("User Current Location");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//
//        currentUserLocationMarker = mMap.addMarker(markerOptions);

        //Move camera with user
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomBy(12));

        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1100);
        locationRequest.setFastestInterval(1100);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}