package com.boss;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ActiveWalk extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentUserLocationMarker;
    private static final int Request_User_Location_Code = 99;
    Cursor cursor;

    String sTrailName;

    PolylineOptions pTrailOptions[][] = {
        {
            // Mountain Trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.320067, 149.027006))
                .add(new LatLng(-35.319157, 149.026901))
                .add(new LatLng(-35.319008, 149.026971))
                .add(new LatLng(-35.318627, 149.027963))
                .add(new LatLng(-35.318474, 149.029106))
                .add(new LatLng(-35.318540, 149.029503))
                .add(new LatLng(-35.318641, 149.029621))
                .add(new LatLng(-35.318799, 149.029707))
                .add(new LatLng(-35.319018, 149.029723))
                .add(new LatLng(-35.319197, 149.029723))
                .add(new LatLng(-35.319455, 149.029594))
                .add(new LatLng(-35.319709, 149.029358))
                .add(new LatLng(-35.320077, 149.028387))
                .add(new LatLng(-35.319836, 149.028264))
                .add(new LatLng(-35.319748, 149.028200))
                .add(new LatLng(-35.319761, 149.028093))
                .add(new LatLng(-35.319857, 149.028023))
                .add(new LatLng(-35.319975, 149.028034))
                .add(new LatLng(-35.320408, 149.028168))
                .add(new LatLng(-35.320636, 149.028120))
                .add(new LatLng(-35.321034, 149.027833))
                .add(new LatLng(-35.321235, 149.027567))
                .add(new LatLng(-35.321248, 149.027326))
                .add(new LatLng(-35.321160, 149.027079))
                .add(new LatLng(-35.321022, 149.026918))
                .add(new LatLng(-35.320067, 149.027006)),
            // Dirt Trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.320837, 149.024906))
                .add(new LatLng(-35.320644, 149.022353))
                .add(new LatLng(-35.320701, 149.021871))
                .add(new LatLng(-35.320627, 149.021420))
                .add(new LatLng(-35.320334, 149.020985))
                .add(new LatLng(-35.318777, 149.019848))
                .add(new LatLng(-35.317219, 149.019194))
                .add(new LatLng(-35.316846, 149.019128))
                .add(new LatLng(-35.315934, 149.020126))
                .add(new LatLng(-35.316266, 149.020977))
                .add(new LatLng(-35.316304, 149.022591))
                .add(new LatLng(-35.316176, 149.023781))
                .add(new LatLng(-35.316229, 149.024553))
                .add(new LatLng(-35.316824, 149.025304))
                .add(new LatLng(-35.320334, 149.020985))
        },
        {
            // Refectory trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.238467, 149.084275))
                .add(new LatLng(-35.236984, 149.084243))
                .add(new LatLng(-35.236937, 149.084299))
                .add(new LatLng(-35.236781, 149.084300))
                .add(new LatLng(-35.236746, 149.084382))
                .add(new LatLng(-35.236537, 149.084489)),
            // Bus stop to library trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.242317, 149.086260))
                .add(new LatLng(-35.242321, 149.085423))
                .add(new LatLng(-35.241509, 149.085379))
                .add(new LatLng(-35.241507, 149.085245))
                .add(new LatLng(-35.241351, 149.085245))
                .add(new LatLng(-35.241296, 149.085173))
                .add(new LatLng(-35.241268, 149.084240))
                .add(new LatLng(-35.240760, 149.084261))
                .add(new LatLng(-35.240561, 149.084280))
                .add(new LatLng(-35.240171, 149.084247))
                .add(new LatLng(-35.239724, 149.084263))
                .add(new LatLng(-35.238563, 149.084187))
                .add(new LatLng(-35.238521, 149.083986))
                .add(new LatLng(-35.238260, 149.083715))
                .add(new LatLng(-35.238085, 149.083691))
        },
        {
            // Gravel Trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.291870, 149.064191))
                .add(new LatLng(-35.291920, 149.064188))
                .add(new LatLng(-35.291954, 149.064179))
                .add(new LatLng(-35.291990, 149.064172))
                .add(new LatLng(-35.292034, 149.064167))
                .add(new LatLng(-35.292073, 149.064180))
                .add(new LatLng(-35.292117, 149.064221))
                .add(new LatLng(-35.292143, 149.064276))
                .add(new LatLng(-35.292166, 149.064357))
                .add(new LatLng(-35.292179, 149.064507))
                .add(new LatLng(-35.292182, 149.064558))
                .add(new LatLng(-35.292068, 149.064563))
                .add(new LatLng(-35.292182, 149.064558))
                .add(new LatLng(-35.292226, 149.064604))
                .add(new LatLng(-35.292283, 149.064628))
                .add(new LatLng(-35.292379, 149.064634))
                ,

            // Grass Trail
            new PolylineOptions().color(Color.rgb(247, 178, 57))
                .add(new LatLng(-35.286953, 149.068193))
                .add(new LatLng(-35.286912, 149.068570))
                .add(new LatLng(-35.286988, 149.068744))
                .add(new LatLng(-35.286880, 149.068921))
                .add(new LatLng(-35.287107, 149.069086))
                .add(new LatLng(-35.286826, 149.069139))
                .add(new LatLng(-35.287084, 149.069279))
                .add(new LatLng(-35.286799, 149.069363))
                .add(new LatLng(-35.287076, 149.069478))
                .add(new LatLng(-35.286826, 149.069588))
                .add(new LatLng(-35.287083, 149.069653))
                .add(new LatLng(-35.286864, 149.069788))
                .add(new LatLng(-35.287090, 149.069825))
                .add(new LatLng(-35.286917, 149.069959))
                .add(new LatLng(-35.287121, 149.069999))
                .add(new LatLng(-35.287168, 149.069936))
                .add(new LatLng(-35.287475, 149.069960))
                .add(new LatLng(-35.287736, 149.070075))
                .add(new LatLng(-35.287880, 149.070142))
                .add(new LatLng(-35.288138, 149.070155))
                .add(new LatLng(-35.288296, 149.070254))
                .add(new LatLng(-35.288296, 149.070254))
                .add(new LatLng(-35.288909, 149.069753))
                .add(new LatLng(-35.289102, 149.069732))
                .add(new LatLng(-35.289426, 149.070161))
                .add(new LatLng(-35.289772, 149.070362))
                .add(new LatLng(-35.290111, 149.070239))
                .add(new LatLng(-35.290330, 149.069872))
                .add(new LatLng(-35.290912, 149.068526))
                .add(new LatLng(-35.291098, 149.068335))
                .add(new LatLng(-35.288473, 149.067955))
        }
    };
    final LatLng pPosition[] = {new LatLng(-35.320068, 149.025592), new LatLng(-35.2384551, 149.0844455), new LatLng(-35.287934, 149.069272)};

    boolean started = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_walk);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Get extras
        Intent recieveIntent = getIntent();
        final String userEmail = recieveIntent.getStringExtra("EMAIL");
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        // get walkedID
        final int walkerID = db.getUserID(userEmail);
        // get walkID
        final String walkID = recieveIntent.getStringExtra("WALK_ID");
        db.open();

        final TextView trailName = findViewById(R.id.textViewTrailName);
        final TextView trailTime = findViewById(R.id.textViewWalkTime);
        // Get walking trail details
        cursor = db.getRegWalksTrailDetails(walkerID,walkID);
        if(cursor.getCount() == 0) {
            Toast.makeText(ActiveWalk.this, "Something went wrong!",
                    Toast.LENGTH_SHORT).show();
        } else {
            while(cursor.moveToNext()) {
                sTrailName = cursor.getString(0);
                trailName.setText(sTrailName);
                trailTime.setText(cursor.getString(1) + " mins");
            }
        }

        final CardView cardViewAction = findViewById(R.id.cardViewAction);
        final TextView textViewAction = findViewById(R.id.textViewAction);

        final CardView cardViewCancel = findViewById(R.id.cardViewCancel);
        final TextView textViewCancel = findViewById(R.id.textViewCancel);

        cardViewAction.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
            // Set time format
            // time format to be stored in database
            SimpleDateFormat dbStored = new SimpleDateFormat("HHmm");
            // time format to display to user
            SimpleDateFormat finished = new SimpleDateFormat("HH:mm");

            if (started) {
                // get current time
                Date currentTime = Calendar.getInstance().getTime();
                // get finished time in our dedicated time format
                String finishedTime = dbStored.format(currentTime);
                // update table column to say 'completed'
                Boolean updateWalkStatusFinished = db.updateWalkStatusFinished(walkerID,finishedTime,walkID);
                if (updateWalkStatusFinished) {
                    Toast.makeText(ActiveWalk.this, "Walk sucessfully finished",
                            Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra("EMAIL", userEmail);
                startActivity(intent);
            } else {
                cardViewAction.setCardBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                // Get current time for start time
                Date currentTime = Calendar.getInstance().getTime();
                // format start time to our dedicated time format
                String startTime = dbStored.format(currentTime);
                // get Trail Duration
                cursor = db.getRegWalksTrailDetails(walkerID,walkID);
                // Move to first row
                cursor.moveToFirst();
                // Set duration
                String duration  = cursor.getString(1);
                // create calendar instance
                Calendar calendar = Calendar.getInstance();
                // set current time
                calendar.setTime(currentTime);
                // add duration to the current time
                calendar.add(Calendar.MINUTE, Integer.parseInt(duration));
                // declare finished time string to be displayed
                String finishedTime = finished.format(calendar.getTime());
                // update table column to say 'started'
                Boolean updateWalkStart = db.updateWalkStatusStart(walkerID,startTime,walkID);
                if (updateWalkStart) {
                    Toast.makeText(ActiveWalk.this, "Starting walk",
                            Toast.LENGTH_SHORT).show();
                    textViewAction.setText("Finish");
                    cardViewCancel.setCardBackgroundColor(Color.DKGRAY);
                    textViewCancel.setText("Stop");
                    trailTime.setText("Expected Finish Time: " + finishedTime);

                    // Walk return time notification
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Intent notificationIntent = new Intent(ActiveWalk.this, WarningReceiver.class);
                    notificationIntent.putExtra("EMAIL", userEmail);
                    PendingIntent broadcast = PendingIntent.getBroadcast(ActiveWalk.this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Calendar cal = Calendar.getInstance();
                    int warningTime = Integer.parseInt(duration) + 30; //Duration of walk plus 30 mins
                    //TODO remove testing line and uncomment actual warning time line
//                    cal.add(Calendar.MINUTE, warningTime);
                    cal.add(Calendar.SECOND, 20);
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                }
                started = true;
            }
            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            if (started) {
                // update table column to say 'incomplete'
                Boolean updateWalkStatusIncomplete = db.updateWalkStatusIncomplete(walkerID,walkID);
                if(updateWalkStatusIncomplete) {
                    Toast.makeText(ActiveWalk.this, "Incompleted walk",
                            Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra("EMAIL", userEmail);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra("EMAIL", userEmail);
                startActivity(intent);
            }
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(mMap.MAP_TYPE_SATELLITE);
//        mMap.setMapType(mMap.MAP_TYPE_TERRAIN);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();

            mMap.setMyLocationEnabled(true);
        }

        // Draw trail and position map to park
        LatLng position;
        PolylineOptions trail;
        final String tNames[][] = {{"Mountain Trail", "Dirt Trail"}, {"Refectory Trail", "Bus To Library Trail"}, {"Gravel Trail", "Grass Trail"}};

        switch (sTrailName) {
            case "Mountain Trail":
                position = pPosition[0];
                trail = pTrailOptions[0][0];
                break;
            case "Dirt Trail":
                position = pPosition[0];
                trail = pTrailOptions[0][1];
                break;
            case "Refectory Trail":
                position = pPosition[1];
                trail = pTrailOptions[1][0];
                break;
            case "Bus To Library Trail":
                position = pPosition[1];
                trail = pTrailOptions[1][1];
                break;
            case "Gravel Trail":
                position = pPosition[2];
                trail = pTrailOptions[2][0];
                break;
            default:
                position = pPosition[2];
                trail = pTrailOptions[2][1];
                break;
        }

        // Setting initial map position
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 14));

        // Drawing trail on map
        this.mMap.addPolyline(trail);
    }

    public boolean checkUserLoationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Request_User_Location_Code:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (googleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        //Move camera with user
        CameraPosition cameraPosition = CameraPosition.builder()
                .target(latLng)
                .zoom(18)
                .bearing(0)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                2000, null);

        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1100);
        locationRequest.setFastestInterval(1100);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}