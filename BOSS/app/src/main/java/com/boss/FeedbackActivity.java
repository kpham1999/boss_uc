package com.boss;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FeedbackActivity extends AppCompatActivity {
    Spinner spinnerHike;
    Cursor cursor;
    String userTrail[];
    CardView cardViewCompleteHikeFeedback;
    EditText editTextRating, editTextFeedback, editTextTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        // Get user email from previous activity
        Intent receiveIntent = getIntent();
        final String userEmail = receiveIntent.getStringExtra("EMAIL");
        // Declare drop down menu / others
        spinnerHike = (Spinner) findViewById(R.id.spinnerHike);
        editTextRating = (EditText) findViewById(R.id.editTextRating);
        editTextFeedback = (EditText) findViewById(R.id.editTextFeedback);
        editTextTime = (EditText) findViewById(R.id.editTextTime);
        cardViewCompleteHikeFeedback = (CardView) findViewById(R.id.cardViewCompleteHikeFeedback);
        // Get calender time
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        // Set onclick to display interactive clock
        editTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(FeedbackActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        editTextTime.setText(pad(hourOfDay) + ":" + pad(minute));
                    }
                },hour,minute,android.text.format.DateFormat.is24HourFormat(FeedbackActivity.this));
                timePickerDialog.show();
            }
        });
        // Open database
        db.open();
        // get UserTrails query table
        int walkerID = db.getUserID(userEmail);
        cursor = db.getUserTrails(walkerID);
        // If there is no rows in the table
        if (cursor.getCount() == 0) {
            Toast.makeText(FeedbackActivity.this, "Sorry you haven't done any hikes!",
                    Toast.LENGTH_SHORT).show();
            // disable if user has no registered trails
            cardViewCompleteHikeFeedback.setEnabled(false);
        } else {
            userTrail = new String[cursor.getCount()];
            cursor.moveToFirst();
            for (int i = 0; i < userTrail.length; i++) {
                // Display trail name
                userTrail[i] = cursor.getString(0);
                cursor.moveToNext();
            }
            // Declare array adapter to show values
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>
                    (this, android.R.layout.simple_list_item_1, userTrail);
            cursor.close();
            // set spinner to show array
            spinnerHike.setAdapter(arrayAdapter);
            // Submit button
            cardViewCompleteHikeFeedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Declare variables
                    String trailName = spinnerHike.getSelectedItem().toString();
                    String rating = editTextRating.getText().toString();
                    String timeStamp = editTextTime.getText().toString();
                    String feedback = editTextFeedback.getText().toString();
                    // get TrailID and WalkerID
                    int traildId = db.getTrailID(trailName);
                    int walkerID = db.getUserID(userEmail);

                    // Check if any fields are empty
                    if (timeStamp.isEmpty() || feedback.isEmpty() || rating.isEmpty()) {
                        Toast.makeText(FeedbackActivity.this, "Please fill in all feilds", Toast.LENGTH_SHORT).show();

                    } else {
                        // Check that rating is less then 5
                        if (Integer.parseInt(rating) > 5) {
                            Toast.makeText(FeedbackActivity.this, "Please make sure the rating is less then 5", Toast.LENGTH_SHORT).show();
                        } else {
                            Boolean insertFeedBack = db.insertFeedback
                                    (walkerID, traildId, timeStamp.replace(":",""), rating, feedback);
                            if (insertFeedBack) {
                                // If values are inserted then, the feedback has been received
                                Toast.makeText(FeedbackActivity.this, "Thank you for your feedback!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                intent.putExtra("EMAIL", userEmail);
                                startActivity(intent);
                                db.close();
                            } else {
                                Toast.makeText(FeedbackActivity.this, "Sorry something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }
    }
    // Add extra 0 to 24 hour format time
    public String pad(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + input;
        }
    }
}
