package com.boss;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAccess {
    private static DatabaseAccess instance;
    Cursor cursor;
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;

    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);

    }
    // return instance
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    // open the database
    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    // close the database
    public void close() {
        if (db != null) {
            this.db.close();
        }
    }
    // Below will be all th queries
    // Reset active user once they have logged out
    public Boolean resetActive() {
        this.db = openHelper.getWritableDatabase();
        String active = "active";
        ContentValues cvActive = new ContentValues();
        cvActive.put("USER_STATUS","inactive");
        db.update("USERID_TABLE", cvActive, "USER_STATUS = ?", new String [] {active});
        return true;

    }
    public Boolean resetActiveRanger() {
        this.db = openHelper.getWritableDatabase();
        String active = "active";
        String usertype = "ranger";
        ContentValues cvActive = new ContentValues();
        cvActive.put("USER_STATUS","inactive");
        db.update("USERID_TABLE", cvActive, "USER_STATUS = ?  AND USER_TYPE = ?",
                new String [] {active,usertype});
        return true;

    }

    // Inserting registration data
    public Boolean insertRegData(String name, String phone, String emergencyContact, String email,
                                 String password, String specialInfo) {
        this.db = openHelper.getWritableDatabase();
        // Define content value for BUSHWALKER_TABLE
        ContentValues cv = new ContentValues();
        // Defined different content value for PASSWORD_TABLE
        ContentValues cvPassword = new ContentValues();
        // Define different content value for USERID_TABLE
        ContentValues cvUserType = new ContentValues();
        // Potential security risk adding this as hard coded
        cvUserType.put("USER_TYPE", "walker");
        cvUserType.put("USER_EMAIL", email);
        // Define table where the data will be stored
        cv.put("WALKER_NAME", name);
        cv.put("WALKER_PHONE", phone);
        cv.put("EMERGENCY_CONTACT", emergencyContact);
        cvPassword.put("COLUMN_PASSWORD", password);
        //cvPassword.put("COLUMN_SALT",salt);
        cv.put("SPECIAL_INFO", specialInfo);
        // Define WALKER_ID to use for foreign key to PASSWORD_TABLE
        long walkerID = db.insert("BUSHWALKER_TABLE", null, cv);
        // STORE WALKER_ID into COLUMN_USER_PWID (FOREIGN KEY)
        cvPassword.put("COLUMN_USER_PWID", walkerID);
        // STORE WALKER_ID TO COLUMN_USER_PWID into PASSWORD_TABLE
        long userPassID = db.insert("PASSWORD_TABLE", null, cvPassword);
        // STORE WALKER_ID TO USER_TYPE into USERID_TABLE
        long userType = db.insert("USERID_TABLE", null, cvUserType);
        // Check to see if all fields are entered into the database
        return walkerID != -1 || userPassID != -1 || userType != 1;
    }

    // Insert Register Data
    public Boolean insertRegHike(int trailID, String startTime, String endTime, String groupSize,
                                 String specInfo, int bwid, long duration)
    {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvRegHike = new ContentValues();
        // Insert data in REG_WALKS table
        cvRegHike.put("COLUMN_BWID", bwid);
        cvRegHike.put("COLUMN_TRAIL_TRAILID",trailID);
        cvRegHike.put("COLUMN_START_TIME", startTime);
        cvRegHike.put("COLUMN_GROUP_SIZE", groupSize);
        cvRegHike.put("COLUMN_BW_EXPECTED_END_TIME", endTime);
        cvRegHike.put("COLUMN_SPECIAL_INFO", specInfo);
        cvRegHike.put("COLUMN_DURATION_EXP",duration);
        // declare PK for data to be inserted
        long walkID = db.insert("REG_WALKS",null,cvRegHike);

        return  walkID != 1;
    }
    // Insert feedback into Comments table
   public Boolean insertFeedback(int walkerID, int trailID, String timeStamp, String rating, String text) {
       this.db = openHelper.getWritableDatabase();
       ContentValues cvFeedBack = new ContentValues();
       // Insert data in REG_WALKS table
       cvFeedBack.put("COLUMN_WALKERID", walkerID);
       cvFeedBack.put("COLUMN_TRAILID", trailID);
       cvFeedBack.put("COLUMN_TIMESTAMP", timeStamp);
       cvFeedBack.put("COLUMN_RATING", rating);
       cvFeedBack.put("COLUMN_COMMENT", text);

       long commentID = db.insert("COMMENTS",null, cvFeedBack);

       return commentID !=1;
    }

    public Boolean updateWalkStatusStart(int walkerID, String time, String walkID) {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvActive = new ContentValues();
        cvActive.put("COLUMN_WALK_STATUS","started");
        cvActive.put("COLUMN_REAL_START_TIME", time);
        db.update("REG_WALKS", cvActive, "COLUMN_BWID = ? AND COLUMN_WALKID = ?",
                new String [] {String.valueOf(walkerID), walkID});
        return true;
    }

    public Boolean updateWalkStatusFinished(int walkerID, String time, String walkID) {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvActive = new ContentValues();
        cvActive.put("COLUMN_WALK_STATUS","completed");
        cvActive.put("COLUMN_REAL_END_TIME", time);
        db.update("REG_WALKS", cvActive, "COLUMN_BWID = ? AND COLUMN_WALKID = ?",
                new String [] {String.valueOf(walkerID), walkID});
        return true;
    }

    public Boolean updateWalkStatusIncomplete(int walkerID, String walkID) {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvActive = new ContentValues();
        cvActive.put("COLUMN_WALK_STATUS","incomplete");
        db.update("REG_WALKS", cvActive, "COLUMN_BWID = ? AND COLUMN_WALKID = ?",
                new String [] {String.valueOf(walkerID), walkID});
        return true;
    }

    // get user data to pre-poulate text fields for registration form
    // (TH: Name, ID, Phone, Emergency Contact, Speccial Info, Trail Name)
    public Cursor setUserRegData(int trailId) {
        this.db = openHelper.getWritableDatabase();
        String active = "active";
        cursor = db.rawQuery("SELECT WALKER_NAME, WALKER_ID, WALKER_PHONE, EMERGENCY_CONTACT, SPECIAL_INFO, TRAIL_NAME FROM BUSHWALKER_TABLE, USERID_TABLE, TRAILS_TABLE " +
                "WHERE USER_ID = WALKER_ID AND USER_STATUS = ? AND TRAIL_ID = ?",new String[]{active,String.valueOf(trailId)});
        return cursor;
    }
    public Cursor setUserActData(int id) {
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT WALKER_PHONE, EMERGENCY_CONTACT, SPECIAL_INFO, WALKER_NAME," +
                "USER_EMAIL " +
                "FROM BUSHWALKER_TABLE, USERID_TABLE " +
                "WHERE WALKER_ID = ? AND USER_ID = ?",new String[] {String.valueOf(id), String.valueOf(id)});
        return cursor;
    }

    // Update user status to active
    public Boolean updateActive(String email) {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvActive = new ContentValues();
        cvActive.put("USER_STATUS","active");
        db.update("USERID_TABLE", cvActive, "USER_EMAIL = ?", new String [] {email});
        return true;
    }

    // Insert updated information into the db
    public Boolean updateInformation(String phone, String emergency, String medical, int id) {
        this.db = openHelper.getWritableDatabase();
        ContentValues cvUpdateData = new ContentValues();
        cvUpdateData.put("WALKER_PHONE",phone);
        cvUpdateData.put("EMERGENCY_CONTACT",emergency);
        cvUpdateData.put("SPECIAL_INFO",medical);
        db.update("BUSHWALKER_TABLE",cvUpdateData,"WALKER_ID = ?",
                new String[]{String.valueOf(id)});

        return true;
    }
    // Get user status if they are active
    public Boolean isActive(String email) {
        String active = "active";
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT * FROM USERID_TABLE " +
                "WHERE USER_EMAIL = ? AND USER_STATUS = ? ", new String[]{email,active});
        return cursor.getCount() > 0;
    }

    // Check email to see if the user exist or not
    public Boolean checkUserExist(String email) {
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT * FROM USERID_TABLE WHERE USER_EMAIL = ?",
                new String[]{email});
        return cursor.getCount() > 0;
    }

    // Check email and password
    public Boolean checkCreds(String email, String password) {
        this.db = openHelper.getWritableDatabase();
        // SQL to check password
        cursor = db.rawQuery("SELECT * FROM USERID_TABLE, PASSWORD_TABLE " +
                        "WHERE USER_EMAIL = ? " +
                        "AND COLUMN_PASSWORD = ?",
                new String[]{email, password});
        // if value is 1 return true
        return cursor.getCount() > 0;
    }

    // check user type to ensure log in with ranger
    public Boolean checkRanger(String email) {
        this.db = openHelper.getWritableDatabase();
        String ranger = "ranger";
        cursor = db.rawQuery("SELECT USER_TYPE FROM USERID_TABLE " +
                "WHERE USER_EMAIL = ? AND USER_TYPE = ?", new String[] {email,ranger});
        return cursor.getCount() > 0;
    }
    public Boolean checkIsRangerActive() {
        this.db = openHelper.getWritableDatabase();
        String active = "active";
        String ranger = "ranger";
        cursor = db.rawQuery("SELECT * FROM USERID_TABLE " +
                "WHERE USER_TYPE = ?  AND USER_STATUS = ?",
                new String[]{ranger,active});
        return cursor.getCount() > 0;
    }
    // Check walk status
    public Boolean checkWalkStatus(String email) {
        this.db = openHelper.getWritableDatabase();
        String started = "started";
        cursor = db.rawQuery("SELECT * FROM REG_WALKS, USERID_TABLE " +
                "WHERE USER_EMAIL = ? " +
                "AND USER_ID = COLUMN_BWID AND COLUMN_WALK_STATUS = ?",new String[] {email,started});
        return cursor.getCount() > 0;
    }

    // Get name of ranger
    public String getRangerName(String email) {
        String name = "not found";
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT * FROM PARKTHAUTH_TABLE, USERID_TABLE " +
                "WHERE RANGER_ID = USER_ID AND USER_EMAIL = ?", new String[]{email});
        if(cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex("RANGER_NAME"));
        }
        return name;
    }

    // Get name of user
    public String getUserName(String email) {
        String name = "";
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT * FROM BUSHWALKER_TABLE, USERID_TABLE " +
                "WHERE WALKER_ID = USER_ID AND USER_EMAIL = ?", new String[]{email});
        if(cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex("WALKER_NAME"));
        }
        return name;
    }
    // retun sorted walk difficulty from descending order (hardest to easiet) (TH: Trail, Difficulty, Rating)
    public Cursor getSortedDifficulty() {
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT TRAIL_NAME Trail, TRAIL_DIFFICULTY Difficulty, TRAIL_DIF_NAME Rating FROM TRAILS_TABLE " +
                "ORDER BY TRAIL_DIFFICULTY DESC", null);
        return cursor;
    }

    // return popular walk from descending order (Most to least) (TH: Registered, Trail)
    public  Cursor getPopularWalks() {
        this.db = openHelper.getWritableDatabase();
        // Group by the trailid to count the number of walkerrs on the trail
        cursor = db.rawQuery("SELECT COUNT(COLUMN_TRAIL_TRAILID) Registered, TRAIL_NAME Trail FROM REG_WALKS, TRAILS_TABLE " +
                "WHERE COLUMN_TRAIL_TRAILID = TRAIL_ID " +
                "GROUP BY COLUMN_TRAIL_TRAILID " +
                "ORDER BY Registered DESC", null);
        return cursor;
    }

    // return table with number of groups and walkers (TH: NumGroup, NumPeople, TrailName)
    public  Cursor getNumGroupsAndWalker() {
        this.db = openHelper.getWritableDatabase();
        // Group the trailid to count the number of groups and to sum the number of people on the trail
        cursor = db.rawQuery("SELECT COUNT(COLUMN_GROUP_SIZE) NumGroup, " +
                "SUM(COLUMN_GROUP_SIZE) NumPeople, TRAIL_NAME Trail " +
                "FROM REG_WALKS, TRAILS_TABLE " +
                "WHERE COLUMN_TRAIL_TRAILID = TRAIL_ID " +
                "GROUP BY COLUMN_TRAIL_TRAILID ",null);
        return cursor;
    }
    // return table with average time and trail name (TH: Time, TrailName)
    public Cursor getAvgWalkTime() {
        this.db = openHelper.getWritableDatabase();
        // Get average by subtrating end time with start time and rounding it to 1 dp
        cursor = db.rawQuery("SELECT ROUND(AVG(COLUMN_REAL_END_TIME - COLUMN_REAL_START_TIME),1) Time, " +
                "TRAIL_NAME Trail, TRAIL_ID FROM REG_WALKS, TRAILS_TABLE " +
                "WHERE COLUMN_TRAIL_TRAILID = TRAIL_ID " +
                "GROUP BY COLUMN_TRAIL_TRAILID " +
                "ORDER BY Time DESC ", null);
        return cursor;
    }
    // return table with average ratings and trail name (TH: Trail, Average)
    public Cursor getAvgTrailRatings() {
        this.db = openHelper.getWritableDatabase();
        // Get rating average by 1dp and group by trailID
        cursor = db.rawQuery("SELECT TRAIL_NAME Trail, ROUND(AVG(COLUMN_RATING),1) Rating " +
                "FROM BUSHWALKER_TABLE, COMMENTS, TRAILS_TABLE " +
                "WHERE COLUMN_TRAILID = TRAILS_TABLE.TRAIL_ID AND COLUMN_WALKERID = WALKER_ID " +
                "GROUP BY COLUMN_TRAILID " +
                "ORDER BY Rating DESC", null);
        return cursor;
    }
    // Get trail details using trailID
    public Cursor getTrailDetails(int id) {
        this.db = openHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT TRAIL_DESCRIPTION, TRAIL_NAME, AVG(COLUMN_RATING) Rating, " +
                        "TRAIL_TYPE, TRAIL_DIF_NAME, TRAIL_DISTANCE, TRAIL_DURATION " +
                        "FROM TRAILS_TABLE, COMMENTS " +
                        "WHERE TRAIL_ID = ? AND COLUMN_TRAILID = TRAIL_ID",
                new String [] {String.valueOf(id)});
        return cursor;
    }

    // Get userID by parsing the user email
    public int getUserID(String email) {
        this.db = openHelper.getReadableDatabase();
        int id = -1;
        cursor = db.rawQuery("SELECT USER_ID FROM USERID_TABLE " +
                "WHERE USER_EMAIL = ?",new String[] {email});
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getInt(cursor.getColumnIndex("USER_ID"));
        }
        return id;
    }

    // Get trail ID
    public int getTrailID(String trailname) {
        this.db = openHelper.getReadableDatabase();
        int id = -1;
        cursor = db.rawQuery("SELECT TRAIL_ID FROM TRAILS_TABLE " +
                "WHERE TRAIL_NAME = ?",new String[] {trailname});
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getInt(cursor.getColumnIndex("TRAIL_ID"));
        }
        return id;
    }

    // Get trail names that the user has registered their hike for
    public Cursor getUserTrails(int id) {
        this.db = openHelper.getWritableDatabase();
        String completed = "completed";
        cursor = db.rawQuery("SELECT TRAIL_NAME " +
                "FROM USERID_TABLE, BUSHWALKER_TABLE, REG_WALKS,TRAILS_TABLE " +
                "WHERE USER_ID = COLUMN_BWID " +
                "AND COLUMN_TRAIL_TRAILID = TRAIL_ID " +
                "AND COLUMN_BWID = ? " +
                "AND COLUMN_WALK_STATUS = ?" +
                "GROUP BY TRAIL_NAME ", new String[] {String.valueOf(id),completed});
        return cursor;
    }

    // Get user registered trails
    public Cursor getUserActivityTrail(int id) {
        this.db = openHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT TRAIL_NAME, COLUMN_START_TIME," +
                "COLUMN_DURATION_EXP," +
                "COLUMN_BW_EXPECTED_END_TIME, " +
                "COLUMN_WALK_STATUS, COLUMN_WALKID " +
                "FROM REG_WALKS, TRAILS_TABLE " +
                "WHERE COLUMN_TRAIL_TRAILID = TRAIL_ID " +
                "AND COLUMN_BWID = ? ", new String[]{String.valueOf(id)});
        return cursor;
    }
    // Get active walkers details
    public Cursor getActiveWalkers() {
        this.db = openHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT WALKER_NAME,TRAIL_NAME, SPECIAL_INFO, COLUMN_START_TIME " +
                "FROM REG_WALKS, USERID_TABLE, TRAILS_TABLE, BUSHWALKER_TABLE " +
                "WHERE USER_STATUS = 'active' AND COLUMN_BWID = USER_ID " +
                "AND TRAIL_ID = COLUMN_TRAIL_TRAILID " +
                "AND WALKER_ID = COLUMN_BWID " +
                "ORDER BY COLUMN_START_TIME ASC", null);
        return cursor;
    }

    public Cursor getRegWalksTrailDetails(int walkerID, String walkID) {
        this.db = openHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT TRAIL_NAME, COLUMN_DURATION_EXP, " +
                "TRAIL_ID, COLUMN_TRAIL_TRAILID, COLUMN_WALKID, COLUMN_BWID " +
                "FROM REG_WALKS, TRAILS_TABLE " +
                "WHERE TRAIL_ID = COLUMN_TRAIL_TRAILID AND COLUMN_BWID = ? " +
                "AND COLUMN_WALKID = ?",new String[]{String.valueOf(walkerID),String.valueOf(walkID)});
        return  cursor;
    }
    // Get user feedback from trail
    public Cursor getFeedback() {
        this.db = openHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT WALKER_NAME, TRAIL_NAME, COLUMN_COMMENT, COLUMN_TIMESTAMP " +
                "FROM COMMENTS, TRAILS_TABLE, BUSHWALKER_TABLE " +
                "WHERE COLUMN_WALKERID = WALKER_ID " +
                "AND COLUMN_TRAILID = TRAIL_ID " +
                "ORDER BY TRAIL_ID DESC",null);
        return cursor;
    }

}