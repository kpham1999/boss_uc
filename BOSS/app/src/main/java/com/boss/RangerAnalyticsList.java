package com.boss;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RangerAnalyticsList extends AppCompatActivity {

    // Define indexes for the columns of the returned db tables to be set
    Integer nameIndex;
    Integer subtitleIndex;
    String subtitleLabel;

    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranger_analytics_list);

        TextView textViewAnalyticTitle = (TextView) findViewById(R.id.textViewAnalyticTitle);

        // Define inflater for linear layout
        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout registeredWalks = (LinearLayout) findViewById(R.id.linearLayoutAnalytics);

        // Call DB
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());

        // Get intent extras
        Intent recieveIntent = getIntent();
        String analytic = recieveIntent.getStringExtra("ANALYTIC");

        // Set title
        textViewAnalyticTitle.setText(analytic);

        // Switch statement for different analytics
        switch(analytic) {
            case "Difficulty":
                cursor = db.getSortedDifficulty();
                nameIndex = 0;
                subtitleIndex = 2;
                subtitleLabel = "Difficulty: ";
                break;
            case "Popular Routes":
                cursor = db.getPopularWalks();
                nameIndex = 1;
                subtitleIndex = 0;
                subtitleLabel = "Most Registered Walks: ";
                break;
            case "Walkers/Group Ratio":
                cursor = db.getNumGroupsAndWalker();
                nameIndex = 2;
                subtitleLabel = "Walkers/Group: ";
                break;
            case "Average Walk Times":
                cursor = db.getAvgWalkTime();
                nameIndex = 1;
                subtitleIndex = 0;
                subtitleLabel = "Average Time (mins): ";
                break;
            case "Trail Ratings":
                cursor = db.getAvgTrailRatings();
                nameIndex = 0;
                subtitleIndex = 1;
                subtitleLabel = "Average Rating: ";
                break;
            case "Comments":
                cursor = db.getFeedback();
                nameIndex = 0;
                subtitleIndex = 2;
                subtitleLabel = "Feedback ";
                break;
            default:
                cursor = db.getActiveWalkers();
                nameIndex = 0;
                subtitleIndex = 1;
                subtitleLabel = "Trail: ";
                break;
        }

        // declare array
        String trailNames [];
        String trailSubtitle[];
        // turn the array to a cursor array to get values
        trailNames = new String[cursor.getCount()];
        trailSubtitle = new String[cursor.getCount()];
        // move to first row
        cursor.moveToFirst();

        for (int i = 0; i < trailNames.length; i++) {
            final View analyticItem = layoutInflater.inflate(R.layout.analytics_scrolling, null);
            // Components to have values set
            TextView trailName = analyticItem.findViewById(R.id.name);
            TextView trailAnalytic = analyticItem.findViewById(R.id.subtitle);

            // Getting components values
            if (analytic.equals("Walkers/Group Ratio")) {
                trailNames[i] = cursor.getString(nameIndex);
                trailSubtitle[i] = subtitleLabel + cursor.getString(1) + "/" + cursor.getString(0);
            }
            else if (analytic.equals("Active Walkers")) {
                trailNames[i] = cursor.getString(nameIndex) + " - " + cursor.getString(3);
                trailSubtitle[i] = subtitleLabel + cursor.getString(subtitleIndex)+","
                        + " Special info: " + cursor.getString(2);
            } else if(analytic.equals("Comments")) {
                trailNames[i] = cursor.getString(nameIndex) + " - " + cursor.getString(1);
                trailSubtitle[i] = subtitleLabel + "("+cursor.getString(3) + "): " + cursor.getString(subtitleIndex);
            }
            else {
                trailNames[i] = cursor.getString(nameIndex);
                trailSubtitle[i] = subtitleLabel + cursor.getString(subtitleIndex);
            }

            // Setting components
            trailName.setText(trailNames[i]);
            trailAnalytic.setText(trailSubtitle[i]);

            // move to next row
            cursor.moveToNext();

            registeredWalks.addView(analyticItem);
        }
    }
}