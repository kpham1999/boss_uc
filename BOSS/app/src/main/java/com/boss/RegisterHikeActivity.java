package com.boss;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RegisterHikeActivity extends AppCompatActivity {

    CardView cardViewCompleteHikeRegister;
    EditText editTextDeparture, editTextDuration, editTextGroupSize, editTextGroupMedicalInfo,
            editTextPhoneNumber, editTextEmergencyPhone;
    TextView textViewTrail;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_hike);
        Intent receiveIntent = getIntent();
        // Get user email and trailID from previous activity
        final String userEmail = receiveIntent.getStringExtra("EMAIL");
        final int trailID = receiveIntent.getIntExtra("ID", 0);

        editTextDeparture = (EditText) findViewById(R.id.editTextDeparture);
        editTextDuration = (EditText) findViewById(R.id.editTextDuration);
        editTextGroupSize = (EditText) findViewById(R.id.editTextGroupSize);
        editTextGroupMedicalInfo = (EditText) findViewById(R.id.editTextGroupMedicalInfo);
        editTextPhoneNumber = (EditText) findViewById(R.id.editTextPhoneNumber);
        editTextEmergencyPhone = (EditText) findViewById(R.id.editTextEmergencyPhone);
        textViewTrail = findViewById(R.id.textViewTrail);
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        db.open();
        // Get user data to pre-populate text fields and trailID
        cursor = db.setUserRegData(trailID);
        if (cursor.getCount() == 0) {
            Toast.makeText(RegisterHikeActivity.this, "No info!", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                textViewTrail.setText(cursor.getString(5));
                editTextPhoneNumber.setText(cursor.getString(2));
                editTextEmergencyPhone.setText(cursor.getString(3));
                editTextGroupMedicalInfo.setText(cursor.getString(4));
                // Set default value to 1 as no group can be below
                editTextGroupSize.setText("1");
            }
        }
        cursor.close();
        db.close();
        // Get calender
        final Calendar calendar = Calendar.getInstance();
        // get hours and minutes
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        // Set onclick to display interactive clock
        editTextDeparture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(RegisterHikeActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        editTextDeparture.setText(pad(hourOfDay) + ":" + pad(minute));
                    }
                },hour,minute,android.text.format.DateFormat.is24HourFormat(RegisterHikeActivity.this));
                timePickerDialog.show();
            }
        });
        // Set onclick to display interactive clock
        editTextDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(RegisterHikeActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        editTextDuration.setText(pad(hourOfDay) + ":" + pad(minute));
                    }
                },hour,minute,android.text.format.DateFormat.is24HourFormat(RegisterHikeActivity.this));
                timePickerDialog.show();
            }
        });
        // Button to complete hike registration form
        cardViewCompleteHikeRegister = findViewById(R.id.cardViewCompleteHikeReg);
        cardViewCompleteHikeRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String specialInifo = editTextGroupMedicalInfo.getText().toString();
                String startTime = editTextDeparture.getText().toString();
                String endTime = editTextDuration.getText().toString();
                String groupSize = editTextGroupSize.getText().toString();
                Date d1 = null;
                Date d2 = null;
                // date format
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                try {
                    d1 = format.parse(startTime);
                    d2 = format.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // Set time dif
                long diff = d2.getTime() - d1.getTime();
                // Get difference in minutes
                long diffMins = diff/(60*1000);

                final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
                db.open();
                // If fields are empty ask user to enter all fields
                if (startTime.isEmpty() || endTime.isEmpty() || groupSize.isEmpty()) {
                    Toast.makeText(RegisterHikeActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                } else {
                    // Get walkerID
                    int walkerID = db.getUserID(userEmail);

                    // Insert data into Reg_Walks
                    Boolean insertRegHike = db.insertRegHike(trailID, startTime.replace(":",""),
                            endTime.replace(":",""), groupSize, specialInifo,walkerID, diffMins);
                    if (insertRegHike) {
                        // If values are inserted the trail has been registered
                        Toast.makeText(RegisterHikeActivity.this, "Hike registration completed", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        intent.putExtra("EMAIL",userEmail);
                        startActivity(intent);
                    } else {
                        Toast.makeText(RegisterHikeActivity.this, "Hike could not be completed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    // Add extra 0 to 24 hour format time
    public String pad(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + input;
        }
    }
}