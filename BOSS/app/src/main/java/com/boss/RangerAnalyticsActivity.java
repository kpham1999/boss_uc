package com.boss;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.ArrayList;

public class RangerAnalyticsActivity extends AppCompatActivity {
    Cursor cursor;
    CardView cardViewDifficulty, cardViewRoutes, cardViewNoPeople,
            cardViewAvgWalkTime, cardViewRatings, cardViewActiveUsers,cardViewComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranger_analytics);

        // Find button
        cardViewDifficulty = (CardView) findViewById(R.id.cardViewDifficulty);
        cardViewRoutes = (CardView) findViewById(R.id.cardViewRoutes);
        cardViewNoPeople = (CardView) findViewById(R.id.cardViewNoPeople);
        cardViewAvgWalkTime = (CardView) findViewById(R.id.cardViewAvgWalkTime);
        cardViewRatings = (CardView) findViewById(R.id.cardViewRatings);
        cardViewActiveUsers = (CardView) findViewById(R.id.cardViewActiveUsers);
        cardViewComments = (CardView) findViewById(R.id.cardViewComments);


        cardViewDifficulty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Difficulty");
            }
        });

        cardViewRoutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Popular Routes");
            }
        });

        cardViewNoPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Walkers/Group Ratio");
            }
        });

        cardViewAvgWalkTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Average Walk Times");
            }
        });

        cardViewRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Trail Ratings");
            }
        });

        cardViewActiveUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Active Walkers");
            }
        });
        cardViewComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAnalytic("Comments");
            }
        });
    }

    public void launchAnalytic(String analytic) {
        Intent intent = new Intent(getApplicationContext(),RangerAnalyticsList.class);
        intent.putExtra("ANALYTIC", analytic);
        startActivity(intent);
    }

}