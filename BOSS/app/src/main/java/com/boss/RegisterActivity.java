package com.boss;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Random;

public class RegisterActivity extends AppCompatActivity {
    public static String bcryptHashString;
    EditText editTextName, editTextEmailAddress, editTextPassword, editTextRepassword, editTextPhone,
            editTextEmergencyContact, editTextSpecialInfo;
    CardView cardViewSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);
        // Text fields
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextEmailAddress = (EditText) findViewById(R.id.editTextEmailAddress);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextRepassword = (EditText) findViewById(R.id.editTextRepassword);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextEmergencyContact = (EditText) findViewById(R.id.editTextEmergencyContact);
        editTextSpecialInfo = (EditText) findViewById(R.id.editTextSpecialInfo);

        // Button
        cardViewSubmit = (CardView) findViewById(R.id.buttonSubmit);

        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());

        // Submit
        cardViewSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Get text from all fields
                String name = editTextName.getText().toString();
                String email = editTextEmailAddress.getText().toString().toLowerCase();
                String password = editTextPassword.getText().toString();
                String repassword = editTextRepassword.getText().toString();
                String phone = editTextPhone.getText().toString();
                String emergencyContact = editTextEmergencyContact.getText().toString();
                String specialInifo = editTextSpecialInfo.getText().toString();

                // Check if any fields are blank ask the user to enter in all the fields
                if (name.isEmpty() || email.isEmpty() || password.isEmpty() || repassword.isEmpty() ||
                        phone.isEmpty()|| emergencyContact.isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "Please enter all the fields",
                            Toast.LENGTH_SHORT).show();
                } else if(password.length() <= 9) {
                    Toast.makeText(RegisterActivity.this,
                            "Please enter a longer password it must be longer then 9 characters",
                            Toast.LENGTH_SHORT).show();
                } else if(!email.contains("@")) {
                    Toast.makeText(RegisterActivity.this,
                            "Please enter a valid email address",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    // Check if password and re-entered password matches or not
                    if (password.equals(repassword)) {
                        // Check if user already exist
                        Boolean checkUser = db.checkUserExist(email);
                        if (!checkUser) {
                            // Create hash of password
                            String hashedPassword = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
                            // Insert data into database
                            Boolean insert = db.insertRegData(name, phone, emergencyContact, email, hashedPassword, specialInifo);
                            if (insert) {
                                Toast.makeText(RegisterActivity.this, "Register successfully",
                                        Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                // Move user to login page
                                startActivity(intent);
                            } else {
                                Toast.makeText(RegisterActivity.this, "Registration failed",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(RegisterActivity.this, "You already exist! Please sign in!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "Password do not match!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}