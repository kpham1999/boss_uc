package com.boss;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

public class TrailDetailsActivity extends AppCompatActivity {
    Cursor cursor;
    TextView trailDescription, trailName, trailType, trailDifficulty, trailDistance, trailDuration;
    RatingBar trailRating;
    Toolbar toolbar;
    CardView cardViewRegisterHike;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trail_details);
        trailDescription = findViewById(R.id.textViewTrailDescription);
        trailName = findViewById(R.id.textViewTrail);
        trailRating = findViewById(R.id.ratingBar);
        trailType = findViewById(R.id.textViewTrailType);
        trailDifficulty = findViewById(R.id.textViewDifficulty);
        trailDistance = findViewById(R.id.textViewDistance);
        trailDuration = findViewById(R.id.textViewDuration);
        toolbar = findViewById(R.id.toolbar);
        cardViewRegisterHike = findViewById(R.id.cardViewRegisterHike);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        // Get ID of trail that was clicked
        final int trailID = intent.getIntExtra("ID", 0);
        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        // parse trail id to get trail information
        cursor = db.getTrailDetails(trailID);
        if (cursor.getCount() == 0) {
            Toast.makeText(TrailDetailsActivity.this, "No trail!", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                trailDescription.setText(cursor.getString(0));
                trailName.setText(cursor.getString(1));
                trailRating.setRating(cursor.getInt(2));
                trailType.setText(cursor.getString(3));
                trailDifficulty.setText(cursor.getString(4));
                trailDistance.setText(cursor.getString(5));
                trailDuration.setText(cursor.getString(6));
            }
            cursor.close();
            db.close();
        }
        // Button to start register hike
        cardViewRegisterHike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent recieveIntent = getIntent();
                String email = recieveIntent.getStringExtra("EMAIL");
                Intent intent = new Intent(getApplicationContext(),RegisterHikeActivity.class);
                intent.putExtra("ID", trailID);
                intent.putExtra("EMAIL", email);
                startActivity(intent);
            }
        });

    }

}